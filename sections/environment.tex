%! TEX root = ../main.tex
\documentclass[../main.tex]{subfiles}


\begin{document}
  \chapter{Środowisko CUDA}
  \label{chap:environment}

  \newcommand{\directive}[1]{\texttt{\_\_#1\_\_}}
  \newcommand{\mycard}{GTX 770}

  \section{Technologia CUDA}
    \subsection{Cel technologii i~jej właściwości}
      Jak opisano w~rozdziale \ref{chap:neural-networks}., jednym z~czynników, który przyczynił się do znaczącego zainteresowania sieciami neuronowymi, było wprowadzenie na rynek konsumencki budżetowych kart graficznych z~procesorami wielorównoległymi. Dzięki nim możliwe stało się wielokrotne przyspieszenie obliczeń niezbędnych do uczenia sieci neuronowych, względem tych przeprowadzanych na CPU.

      Pierwszą kartą graficzną wspomnianego typu był GeForce 256 wyprodukowany przez firmę Nvidia \cite{Nvidia2019}. Jego początkowym, jedynym zastosowaniem było zrównoleglone wyliczanie przekształceń, oświetlenia oraz ustawień trójkątów w~grafice trójwymiarowej. W~związku z~tym, karty graficzne przeznaczone były głównie na rynek gier komputerowych. Aby móc zaprogramować wykonanie dowolnej operacji matematycznej na GPU, należało wykorzystać możliwości programowania GPGPU (z ang. \textit{General-Purpose Computing on Graphics Processing Units}). Programowanie na GPU odbywało się poprzez implementację krótkich programów komputerowych o~nazwach \textit{Vertex Shader} oraz \textit{Pixel Shader}. Pierwszy z~nich służył do przetwarzania informacji o~położeniu wierzchołków. Drugi natomiast określał wartość pikseli wyświetlanych na ekranie na podstawie ustawień sceny trójwymiarowej. Aby stworzyć program o~dowolnie innym przeznaczeniu niż wyświetlanie grafiki, należało napisać tak implementację, żeby wykorzystać abstrakcje dostępne w~takich programach cieniowania. Dla przykładu, aby móc wyliczyć symulację rozprzestrzeniania się ciepła w~pokoju z~umieszczonym w~nim kaloryferem, konieczne było zaprogramowanie procesu w~postaci transformacji geometrii, a~same wartości występujące w~danym miejscu jako wartości pikseli. Dopiero 8~listopada 2006 roku premierę miała technologia CUDA (z ang. \textit{Compute Unified Device Architecture}) \cite{Nvidia2019a}, pozwalająca w~prosty sposób implementować obliczenia na karcie graficznej. API dołączone do technologii, w~postaci zmodyfikowanej wersji języka C, pozwoliło w~prosty sposób definiować operacje w~postaci specjalnie oznaczonych funkcji w~kodzie źródłowym. Następnie, te funkcje były kompilowane za pomocą kompilatora NVCC, a~w czasie uruchomienia programu dystrybuowane przez kontroler zainstalowany na karcie na wiele rdzeni CUDA (z ang. \textit{CUDA Cores}). Taki model programowania nazwano programowaniem heterogenicznym, w~którym programista może w~jednym pliku źródłowym pisać kod wykonujący się na CPU i~wywołujący funkcję uruchamianą na GPU. Mechanizm takiego programowania przedstawiono na rysunku \ref{fig:heterogenous-programming-model}. To otworzyło drzwi na wiele nowych zastosowań kart graficznych w~tym symulacje fizyczne, szyfrowanie, renderowanie rzeczywistości wirtualnej w~medycynie, aż w~końcu w~samej sztucznej inteligencji. W~późniejszym czasie, zaczęły pojawiać się wysokopoziomowe biblioteki, które ukrywały przed użytkownikiem implementację przeznaczoną dla karty graficznej. Dzięki temu, użytkownik mógł korzystać w~niemal przeźroczysty sposób z~tych samych operacji matematycznych, co na CPU. Przykładem takiej biblioteki jest Eigen \cite{Eigen2010} dla języka C++. Później powstawały również nakładki na CUDA, takie jak cuBLAS \cite{CuBlas2019} posiadający tę samą składnię, co BLAS \cite{Blas2019}, przystosowany do obliczeń wektorowych i~macierzowych przy pomocy instrukcji SIMD. Bardziej specjalizowaną biblioteką jest cuDNN \cite{Cudnn2014}, przeznaczoną konkretnie pod głębokie uczenie maszynowe, udostępniające w~swoim API wiele konstrukcji różnych warstw sieci neuronowych. Najwyższe poziomy abstrakcji programowania zostały osiągnięte przez biblioteki takie jak PyTorch \cite{Pytorch2017}, Caffe \cite{Caffe2014}, Theano \cite{Theano2016}, Tensoflow \cite{Tensorflow2015}, czy będący nakładką na dwa wcześniejsze Keras \cite{Keras2019}. Wymienione biblioteki posiadają interfejsy w~języku Python i~są wykorzystywane w~przytoczonych pracach na temat głębokich modeli.

    \subsection{Architektura}
      U~podstaw modelu programowania równoległego w~CUDA leżą 3~główne abstrakcje: hierarchia wątków połączonych w~grupy bloków tworzących siatkę, współdzielona pamięć oraz barierowa synchronizacja (z ang. \textit{barrier synchronization}) \cite{CudaProgrammingGuide2010}. Wymienione abstrakcje są dostępne dla użytkownika poprzez minimalne rozszerzenie języka programowania C~oraz C++. Zarówno siatka, bloki oraz wątki mogą być nawet 2- i~3-wymiarowej postaci, co pozwala zrównoleglać obliczenia posiadające charakter wielowymiarowy. Wątki wewnątrz bloku mogą ze sobą współpracować przy użyciu specjalnych funkcji barierowych (z ang. \textit{fence functions}) oraz dzielić informacje przy użyciu pamięci współdzielonej. Przykładowa siatka została zwizualizowana na rysunku \ref{fig:grid-architecture}. Bloki w~czasie wykonywania programu są dystrybuowane równomiernie w~miarę dostępnych zasobów na poszczególne, multiprocesorowe jednostki strumieniowania (z ang. \textit{streaming multiprocessor} lub SM). Wewnątrz pojedynczego SM, bloki są wykonywane równolegle, tak samo jak wątki wewnątrz bloków. Jeżeli w~kolejce znajdują się bloki do wykonania, są one przydzielane do pierwszych wolnych SM. Podział na SM został pokazany na rysunku \ref{fig:multiprocessor-architecture}. 


      \begin{figure}[!htb]
        \centering
        \begin{minipage}[t]{0.49\linewidth}
          \centering
          \includegraphics[width=0.99\linewidth]{assets/cuda/grid-architecture.pdf}
          \caption{Wizualizacja siatki elementów w~CUDA, pobrana z~\cite{CudaProgrammingGuide2010}}
          \label{fig:grid-architecture}
        \end{minipage}
        \hfill
        \begin{minipage}[t]{0.49\linewidth}
          \centering
          \includegraphics[width=0.99\linewidth]{assets/cuda/multiprocessor-architecture.pdf}
          \caption{Wizualizacja siatki elementów w~CUDA przy uwzględnieniu multiprocesorów, pobrana z~\cite{CudaProgrammingGuide2010}}
          \label{fig:multiprocessor-architecture}
        \end{minipage}
      \end{figure}

      Multiprocesory zostały zaprojektowane do wykonywania naraz setek wątków. Aby mogło to odbywać się w~ten sposób, zaprojektowano specjalną architekturę instrukcji SIMT (z ang. \textit{Single-Instruction Multiple-Thread}), opisującą zdolność obliczeniową (z ang. \textit{compute capability}). Obecnie, istnieje jej siódma wersja. SIMT przypomina architekturę SIMD. 
      
      Pojedynczy SM tworzy, zarządza, kolejkuje i~uruchamia grupy o~wielkości 32 równoległych wątków o~nazwie \textit{warp}. Wewnątrz takiej grupy, wątki wykonują tę samą ścieżkę instrukcji, niezależnie czy występują warunkowe wyrażenia czy nie. To powoduje, że jeśli część grupy będzie wykonywała ciąg instrukcji znajdujących się w gałęzi spełniającej warunek wyrażenia \code{if}, to pozostała część będzie oczekiwała na zakończenie wykonywania instrukcji przez tę pierwszą. Następnie, wykonywana jest druga ścieżka, jeśli którykolwiek z~wątków ją spełnia. Zjawisko to określa się jako dywergencja wątków. Zatem, dla szybkiego wykonywania programu na GPU, unika się warunkowych wyrażeń lub tworzy się taką funkcję, aby każdy z~wątków w~grupie posiadał tę samą ścieżkę wykonania. Liczba grup przypadających na pojedynczy multiprocesor określa liczba:

      \begin{equation}
        \ceil*{\frac{T}{W_{size}}},
      \end{equation}
      gdzie $T$~to liczba wątków przypadająca na pojedynczy blok, natomiast $W_{size}$ to rozmiar grupy równy 32.
      
      Bloki posiadają 32-bitowe rejestry oraz pamięć współdzieloną pomiędzy grupami. Wielkość pamięci jest zależna od technologii, w~jakiej została wykonana karta. Służy ona do komunikacji danych pomiędzy wątkami wewnątrz bloku. Każdy z~bloków ma rozłączną pamięć. To znaczy, że bloki mogą komunikować się jedynie za pomocą wolniejszej globalnej pamięci. Tak zdefiniowane bloki tworzą siatkę bloków. Programista może odwołać się do poszczególnych elementów siatki poprzez specjalnie zdefiniowane indeksy. Każdy blok w~siatce ma unikalny indeks. Wątki posiadają unikalny indeks tylko wewnątrz bloku, co oznacza, że pomiędzy blokami mogą się one powtarzać. 
      
      W~programowaniu w CUDA, udostępnione są trzy rodzaje pamięci, uszeregowane od najwolniejszej do najszybszej:
      \begin{enumerate}
        \item Globalna - jest to pamięć, do której ma dostęp każdy wątek. Każda dana w~czasie uruchomienia programu GPU znajduje się w~tym rodzaju pamięci;
        \item Współdzielona - jest to znacznie szybszy rodzaj pamięci, który można porównać do pamięci podręcznej L2 dostępnej w~CPU. Pamięć współdzielona jest rozłączna względem bloków. To znaczy, że dana zapisana dla przykładu w~bloku o~indeksie~1~nie będzie dostępna dla wątku z~bloku o~indeksie 2. Dla każdego z~bloków przypada taka sama ilość pamięci podręcznej i~jest ograniczona przez liczbę dostępnej pamięci przypadającej na pojedynczy SM;
        \item Stała - szybkość odczytu z~niej jest na poziomie pamięci współdzielonej. Nie jest możliwe zapisywanie danych do niej w~czasie wywoływania funkcji na GPU;
        \item Lokalna - przypada na pojedynczy wątek. Jest reprezentowana przez zmienne definiowane wewnątrz ciała funkcji, dystrybuowanej na wątki. Istnieje możliwość przesyłania danych pomiędzy poszczególnymi wątkami poprzez specjalne funkcje dostępne w~środowisku CUDA.
      \end{enumerate}
      Przedstawiony model pamięci pokazano na rysunku \ref{fig:memory-architecture}. 
      \begin{figure}[!htb]
        \centering
        \begin{minipage}[t]{0.49\linewidth}
          \centering
          \includegraphics[width=\linewidth]{assets/cuda/memory-architecture.pdf}
          \caption{Wizualizacja modelu pamięci GPU, pobrana z~\cite{CudaProgrammingGuide2010}}
          \label{fig:memory-architecture}
        \end{minipage}
        \hfill
        \begin{minipage}[t]{0.49\linewidth}
          \centering
          \includegraphics[width=\linewidth]{assets/cuda/heterogenous-programming-model.pdf}
          \caption{Wizualizacja przepływu programu w~modelu programowania heterogenicznego dla GPU, pobrana z~\cite{CudaProgrammingGuide2010}}
          \label{fig:heterogenous-programming-model}
        \end{minipage}
      \end{figure}
      
      W~kartach graficznych, występuje także dedykowana pamięć \textit{cache} L2, jednak jest ona zarządzana w~większości przypadków automatycznie przez kartę graficzną. Dostępna jest również pamięć teksturowa, będąca nakładką na pamięć globalną, pierwotnie przeznaczona do grafiki komputerowej. Jej przewaga polega na użyciu funkcji mapującej, gdzie indeksy dostępu do pamięci podawane jako argument, są mapowane na odpowiednie miejsca w~przestrzeni rzeczywistej pamięci globalnej. To pozwala na zoptymalizowanie nieciągłych odwołań dostępu do pamięci, co może w~wielu przypadkach przyspieszyć działanie programu. Przykładem takiego nieciągłego odwołania jest odczyt dwóch sąsiadujących przestrzennie elementów w~kolumnie macierzy, zapisanej jako jednowymiarowy wektor.

      W~literaturze można znaleźć wytyczne dotyczące pisania optymalnego kodu CUDA. Opisywane są one następująco. Największą wydajność można uzyskać w~momencie, jeżeli odczyt z~pamięci globalnej wewnątrz grupy jest ciągły. To znaczy, że dla każdego wątku o~indeksie $i$~powinien zostać wykonany odczyt z~adresu $i + offset$, gdzie $offset$ jest niewielką wartością stałą wewnątrz grupy. Można to wykorzystać, aby każdy z~wątków wewnątrz grupy zapisał dane do pamięci współdzielonej. Wtedy elementy odległe od siebie o~$offset$ w~pamięci globalnej będą ze sobą sąsiadowały w~pamięci współdzielonej. To może dodatkowo przyspieszyć optymalizację odczytu. Przy korzystaniu z~pamięci współdzielonej może wystąpić zjawisko hazardu (z ang. \textit{race condition}). Aby każdy z~wątków w~bloku miał dostęp do tych samych danych w~pamięci współdzielonej, konieczne jest wykonanie synchronizacji wątków. Mimo wymuszenia synchronizacji, odpowiednie użycie pamięci współdzielonej jest szybsze względem ciągłego dostępu do pamięci globalnej. Przykład możliwych do zastosowania optymalizacji pokazano na przykładzie zadania transpozycji macierzy w~\cite{CudaTranspose2019}, gdzie autorowi udało się zoptymalizować naiwną implementację, zwiększając ponad 5-krotnie przepustowość karty Tesla M2050.

      W~tabeli \ref{tab:gpu-comparison}. pokazano przykładowe liczby poszczególnych elementów dostępnych w~czasie programowania w~CUDA dla karty \mycard, wykorzystywanej w~czasie badań, oraz karty GTX 1080 Ti, która jest jedną z~najlepszych pod względem wydajności dostępnych obecnie kart graficznych. 

      \begin{table}[!htb]
        \centering
        \small
        \caption{Wartości parametrów bazowych elementów kart graficznych, wykorzystywanych w czasie programowania w CUDA. Parametry karty stosowanej w~czasie badań w~rozdziale \ref{chap:experiments}. zestawiono z~parametrami przykładowej, topowej karty. Wartości podane w nawiasach oznaczają, że są one niezależne dla każdego z~wymiarów na karcie graficznej.}
        \begin{tabular}{ccc}
          \toprule
            \textbf{Własność} & \mycard & GTX 1080 Ti \\ 
          \midrule
            Dostępna pamięć globalna & 1999 MB & 11264 MB \\
            Dostępna pamięć współdzielona & 49152 B & 49152 B \\
            Dostępna pamięć stała & 65536 B & 65536 B \\
            Liczba multiprocesorów & 8 & 28 \\
            Liczba rdzeni CUDA & 1536 & 3584 \\ 
            Liczba rejestrów na blok & 65536 & 65536 \\ 
            Liczba wątków na blok & 1024 & 1024 \\
            Liczba wątków na multiprocesor & 2048 & 2048 \\
            Rozmiar grupy & 32 & 32 \\
            Wymiary bloków & (1024, 1024, 64) & (1024, 1024, 64) \\
            Wymiary siatki & (2147483647, 65535, 65535) & (2147483647, 65535, 65535) \\
          \bottomrule
          
        \end{tabular}
        \label{tab:gpu-comparison}
      \end{table}

    \subsection{Użycie CUDA}
      Poniżej przedstawiono opis przykładowego programu, wykonującego dodanie dwóch wektorów \texttt{a} oraz \texttt{b} i~ zapisującego rezultat do wektora \texttt{c}. Poniższe przedstawienie programowania w~CUDA to zaledwie skrawek jego możliwości. Więcej szczegółów można znaleźć w~literaturze \cite{CudaProgrammingGuide2010, Cheng2014, Sanders2019}. 
      
      Wewnątrz pliku źródłowego o~rozszerzeniu \texttt{*.cu} tworzona jest odpowiednia funkcja określona dyrektywą \directive{global}, nazywana kernelem. Dyrektywa oznacza funkcję, która może być wykonana na GPU i~dystrybuowana na wiele wątków. W~CUDA dostępne są także następujące dyrektywy dotyczące funkcji:

      \begin{description}
        \item[\directive{host}] - funkcja może być wywołana na CPU
        \item[\directive{device}] - funkcja może zostać wywołana tylko z~poziomu innego kernela
      \end{description}
      Istnieje możliwość jednoczesnego użycia dyrektyw \directive{host} oraz \directive{device}. To pozwala pisać generyczne funkcje, które zależnie od kontekstu będą wykonywały się na CPU lub GPU. Następnie, podawana jest konstrukcja funkcji, której składnia jest taka sama jak w~C. Podawany jest zwracany typ, który jest zawsze \texttt{void}, nazwa oraz argumenty. Argumentami mogą być tylko tablice, skalary oraz struktury \texttt{struct}. Każdy z~nich może być poprzedzony dyrektywą \directive{restrict}, pozwalającą kompilatorowi na zastosowanie odpowiednich optymalizacji. Dyrektywa zapewnia, że nie występuje zjawisko \textit{aliasingu} \cite{CudaAliasing2019}. Ciało funkcji należy traktować jako program, który zostanie przydzielony pojedynczemu wątkowi w~siatce. Mając do dyspozycji odpowiednią liczbę wątków, można określić, aby każdy wątek odpowiadał za odczyt pojedynczych elementów z~tablic \texttt{a} i~\texttt{b}, a~na końcu wykonywał zapis do odpowiedniej lokalizacji w~\texttt{c}. Pojawia się kwestia, jak odwołać się do odpowiedniego elementu tablicy, nie powodując nałożenia się pracy wątków. Przy dodaniu na początku pliku źródłowego nagłówka \texttt{\#include <cuda.h>}, do dyspozycji zostaje oddana możliwość dokładnej lokalizacji wątku w~siatce poprzez globalne pola dostępne z~poziomu kernela:

      \begin{description}
        \item[\code{threadIdx.x}, \code{threadIdx.y}, \code{threadIdx.z}] - każde z~pól określa położenie wątku wewnątrz bloku o~ustalonym indeksie;
        \item[\code{blockIdx.x}, \code{blockIdx.y}, \code{blockIdx.z}] - każde z~pól określa położenie bloku wewnątrz siatki o~stałym rozmiarze;

        \item[\code{blockDim.x}, \code{blockDim.y}, \code{blockDim.z}] - określają wymiarowość każdego z~bloków;
        \item[\code{gridDim.x}, \code{gridDim.y}, \code{gridDim.z}] - określają  wymiarowość siatki.
      \end{description}
      Na podstawie tego można wyliczyć dokładne położenie elementu w~wektorze dla danego wątku: \code{int idx = blockIdx.x *~blockDim.x + threadIdx.x}. Takie odwołanie powoduje, że praca żadnego z~wątków nie będzie się nakładać oraz nie dojdzie do zjawiska hazardu przy zapisie. Zatem, operację dodania elementów i~zapisania w~tablicy wyjściowej można zapisać następująco: \code{c[idx] = a[idx] + b[idx]}. Na końcu należy wywołać zdefiniowany kernel przy użyciu specjalnej konstrukcji \code{{<}<{<}blocks,~threads,~sharedMem,~stream{>}>{>}}, gdzie poszczególne składowe wywołania to:

      \begin{description}
        \item[\code{blocks}] - definicja liczby bloków użytych do wykonania funkcji, przy czym może ona przyjmować wiele wartości poprzez konstrukcję \code{dim3}, co daje możliwość uruchamiania na wielu wymiarach;
        \item[\code{threads}]- definicja liczby wątków przypadających na pojedynczy blok, która może być również zdefiniowana dla kilku wymiarów;
        \item[\code{sharedMem}] - liczba bajtów przeznaczonych dla pojedynczego bloku do wykorzystania przy dynamicznej alokacji pamięci współdzielonej;
        \item[\code{stream}] - indeks strumienia przetwarzania. Strumienie przetwarzania pozwalają na równoległe uruchamianie wielu kerneli niezależnych od siebie, jeżeli są dostępne odpowiednie zasoby na karcie graficznej.
      \end{description}
      Funkcję dodającą dwa elementu wektorów o~nazwie \code{vecAdd} można wywołać w~następujący sposób:
      \begin{lstlisting}
             vecAdd<<<blocks, threads>>>(a, b, c);
      \end{lstlisting} 
      Przed wykonaniem, należy zaalokować tablice na GPU oraz przenieść dane z~CPU na GPU za pomocą odpowiednich funkcji. Istnieje możliwość kopiowania danych w~kierunkach: CPU $\rightarrow$ GPU, GPU $\rightarrow$ CPU, GPU $\rightarrow$ GPU oraz CPU $\rightarrow$ CPU. To znaczy, że w~pierwszej kolejności należy przekopiować dane w~kierunku CPU $\rightarrow$ GPU, wywołać funkcję \code{vecAdd}, przekopiować w~kierunku GPU $\rightarrow$ CPU, a~następnie zdealokować zajmowaną na GPU pamięć. Po wykonaniu tych kroków, w~tablicy \code{c} powinien znajdować się rezultat dodania wektorów \code{a} oraz \code{b}.

      W~kontekście obliczeń na GPU, ważna jest też synchronizacja wątków służąca do niwelowania zjawiska hazardu. Powoduje ona zatrzymanie wszystkich wątków, które zdążyły wykonać instrukcje występujące przed wywołaniem blokady. Jeżeli każdy z~wątków dotrze do blokady, to jest ona zwalniana i~kernel jest wykonywany dalej. Metoda jest szczególnie istotna przy korzystaniu z~pamięci współdzielonej. Współdzieloną pamięć deklaruje się za pomocą dyrektywy \directive{shared} występującej wewnątrz ciała kernela. Alokacja takiej pamięci może być statyczna poprzez deklarację w~postaci \code{\_\_shared\_\_ float sharedMem[nbytes]} lub poprzez podanie liczby bajtów do zaalokowania w~ciele wywołania kernela \code{{<}<{<}...{>}>{>}}. Dane w~takim obszarze mogą być jednego z~typów wbudowanych w~CUDA. Istnieje także dyrektywa \directive{constant}. Przy jej pomocy można zadeklarować w~sposób statyczny globalnie lub lokalnie dostępną pamięć stałą.

    \subsection{Zastosowania}
      Przełomową, w~kontekście wykorzystania karty graficznej w~dziedzinie uczenia maszynowego, była praca A. Krizhevsky'ego \cite{Alexnet2012}. Autor pracy stworzył własną bibliotekę do głębokich modeli, którą wykorzystał w~konkursie \textit{ILSVRC-2012}. Osiągnął w~nim pierwsze miejsce, rozpoczynając tym samym dominację głębokich modeli w~tej serii konkursów. Zaprogramowanie wszystkich operacji sieci na GPU pozwoliło autorowi wyuczyć model posiadający około 60 milionów parametrów w~6 dni na dwóch kartach GTX 580. Obecnie, istnieje wiele wysokopoziomowych bibliotek korzystających ze zoptymalizowanych instrukcji GPU. Z~tego powodu, implementacja tej biblioteki nie jest obecnie wspierana.

      Zastosowanie GPU nie ogranicza się jedynie do dziedziny głębokiego uczenia maszynowego. Autorzy pracy \cite{Stone2008} stworzyli specjalny program wykorzystujący możliwości GPU do odtworzenia zdjęć MRI w~postaci kostki o~wymiarach $128^3$ wokseli. Dzięki zastosowaniu karty graficznej udało im się odtworzyć taką kostkę w~czasie około minuty. Przy zastosowaniu wielordzeniowych procesorów AMD Opteron taki proces trwał powyżej 20 minut. Udało im się dodatkowo zmniejszyć błąd rekonstrukcji z~42\% do 12\%. 

      Za pomocą GPU wykonano także symulację tsunami \cite{Tsunami2019}. W~prezentacji autorzy przyspieszyli jej wykonanie z~czasu 10 godzin na procesorze Intel Xeon do 10 minut na pojedynczej karcie graficznej.

      Użycie kart graficznych można znaleźć również w~tradycyjnych algorytmach widzenia komputerowego. W~pracy \cite{Sakurikar2012} autorzy użyli GPU do przyspieszenia popularnego algorytmu \textit{Graph Cut}. Działa on poprzez znalezienie grafu podobnego do grafu zdefiniowanego przy użyciu losowego pola Markova na obrazie, a~następnie wykorzystanie takiego podgrafu do sparametryzowania grafu głównego i~zmniejszenia go. Przy użyciu autorskiej implementacji udało im się dwukrotnie przyspieszyć działanie metody.

      W~pracy \cite{Barsdell2010} autorzy sprawdzają aplikowalność algorytmów w~znanych problemach astronomicznych: wykrywanie mikrosoczewkowania grawitacyjnego za pomocą odwróconego śledzenia promieni w~celu wykrywania masywnych obiektów astronomicznych, oczyszczanie zdjęć metodą H\"ogboma, renderowania obiektów o~objętości $256^3$ wokseli oraz zintegrowania danych czasowych pochodzących z~badań pulsarów. Przy dwóch różnych architekturach GPU, autorzy odnotowali 100-krotne przyspieszenie względem implementacji dostępnych na CPU.

      Środowisko CUDA jest istotne w~kontekście programów grafiki trójwymiarowej. Wiele programów graficznych 3D wykorzystuje możliwości programowania przy pomocy CUDA API, aby przyspieszyć proces renderowania. Dzięki temu, programy grafiki komputerowej mogą w~czasie rzeczywistym wykonywać obliczenia dotyczące wartości pikseli przy wykorzystaniu techniki \textit{Path Tracing}, odwzorowującej fizyczne zachowanie światła. W~takim programie, możliwe jest zrównoleglenie śledzenia każdej ścieżki światła na pojedynczy wątek GPU. Zaletą tego jest to, że żaden promień nie wchodzi w~interakcję z~innymi, co zapewnia wysoką skalowalność metody. Technologię wykorzystano między innymi w~programie Blender 3D \cite{Blender2019}.

  \section{Biblioteka Tensorflow}
    Biblioteka Tensorflow jest jedną z~popularniejszych wśród stosowanych w~dziedzinie uczenia maszynowego. Na podstawie rysunku \ref{fig:framework-popularity}., można zauważyć, że największą popularnością cieszy się właśnie ona. Przy opracowaniu tych statystyk wzięto pod uwagę wiele czynników:
    \begin{itemize}
      \item liczbę wystąpień danej biblioteki w~ofertach prac,
      \item liczbę wzmianek w~artykułach na serwisie KDnuggets\footnote{\url{https://www.kdnuggets.com/}} oraz Medium\footnote{\url{https://medium.com/}},
      \item liczbę wyszukiwań hasła w~wyszukiwarce Google,
      \item liczbę książek dotyczących danych bibliotek w~serwisie Amazon Books,
      \item aktywność na serwisie GitHub\footnote{\url{https://github.com/}},
      \item liczbę artykułów naukowych korzystających z~danej biblioteki w~serwise ArXiv\footnote{\url{https://arxiv.org/}}. 
    \end{itemize}
    Następnie, wartości te zostały znormalizowane do przedziału $[0, 1]$. Na końcu, wzięto średnią ważoną, gdzie wagi zostały przedstawione na rysunku \ref{fig:popularity-weights}. Dokładna metoda obliczeń została opisana w~\cite{Hale2019}.  

    \begin{figure}[H]
      \centering
      \includegraphics[width=0.99\linewidth]{assets/framework-popularity.pdf}
      \caption{Popularność bibliotek metod uczenia maszynowego według użytkowników}
      \label{fig:framework-popularity}
    \end{figure}

    \begin{figure}[H]
      \centering
      \includegraphics[width=0.7\linewidth]{assets/popularity-weights.pdf}
      \caption{Wagi dla każdej z~kategorii popularności bibliotek metod uczenia maszynowego}
      \label{fig:popularity-weights}
    \end{figure}

    Przedstawiony rezultat pokazuje, że aby badania przeprowadzone w~tej pracy były łatwo reprodukowalne przez użytkowników, należało wykorzystać najpopularniejszą bibliotekę. Dzięki temu, metoda może być testowana również przez innych, co może przyspieszyć jej rozwój.


    \subsection{Architektura}
      Tensorflow jest biblioteką dostępną na wielu systemach operacyjnych. Kod wykonujący obliczenia oraz budujący abstrakcje konieczne do wykonania obliczeń został napisany w~C. Jest on odseparowany od interfejsów napisanych w~Pythonie, C++, Javie i~wielu innych popularnych językach. Stos oprogramowania pokazano na rysunku \ref{fig:tensorflow-stack}.

      \begin{figure}[!htb]
        \centering
        \includegraphics[width=0.6\linewidth]{assets/tensorflow-stack.pdf}
        \caption{Stos oprogramowania Tensorflow}
        \label{fig:tensorflow-stack}
      \end{figure}
      
      Klientem nazywa się program w~określonym języku, który buduje graf obliczeniowy. Cel takiego budowania grafu został opisany w~podrozdziale \ref{subsec:static-graph}. Graf składa się z~indywidualnych, bazowych operacji lub wyższych abstrakcji w~postaci klas i znajduje się w~tak zwanej sesji. Wewnątrz sesji, definicja grafu jest wysyłana do głównej jednostki obliczeniowej (\textit{master}) w~postaci buforu protokołowego (z ang. \textit{protocol buffer}). Kiedy użytkownik wywołuje jeden lub więcej węzłów w~grafie, inicjowane jest wywołanie obliczenia na głównej jednostce obliczeniowej. W~przypadku dostępu do klastra obliczeniowego, \textit{master} służy do dystrybucji zadań na węzły klastra, tak zwane \textit{workery}. \textit{Workery} mogą otrzymać jedną z~dwóch funkcji: \enquote{PS} lub \textit{parameter server}, która zarządza przechowywaniem i~aktualizacją parametrów, oraz \enquote{WS} lub \textit{worker}, odpowiedzialną za wykonywanie obliczeń związanych z~ewaluacją grafu obliczeniowego. Druga z~wymienionych oznacza w~kontekście sieci neuronowych wykonanie przejścia w~przód oraz w~tył. W~przypadku wykonania tych zadań bez dostępu do klastra obliczeniowego, wszystkie funkcje są wykonywane na głównej jednostce \textit{master}. Przykład definicji grafu obliczeniowego po stronie klienta oraz obsługi wywołania obliczenia pokazano na rysunku \ref{fig:computation-graph-and-distribution}.

      \begin{figure}[!htb]
        \centering
        \includegraphics[width=0.8\linewidth]{assets/computation-graph-and-distribution.pdf}
        \caption{Wizualizacja definicji grafu obliczeniowego oraz procesu jego rozpraszania}
        \label{fig:computation-graph-and-distribution}
      \end{figure}
      Główna jednostka wykonuje następujące funkcje:
      \begin{itemize}
        \item przycina graf do minimalnego grafu, koniecznego do ewaluacji węzłów wywołanych przez klienta;
        \item dzieli graf na kawałki i~dystrybuuje je po dostępnych urządzeniach obliczeniowych;
        \item przenosi wyliczone kawałki do pamięci \textit{cache}, aby mogły zostać wykorzystane ponownie.
      \end{itemize}
      Ponieważ jednostka główna widzi wszystkie kroki obliczeniowe, stosuje ona różne optymalizacje. Przykładem takiej optymalizacji jest usuwanie powtarzających się wyrażeń, które mogą być wywołane tylko raz, oraz przenoszenie wielokrotnie używanych zmiennych o~stałej wartości do wspólnej przestrzeni. Następnie, koordynuje tak zoptymalizowanymi podgrafami poprzez rozproszenie zbioru zadań na \textit{workery}. W~miejscu, gdzie następuje przecięcie grafu, główna jednostka umieszcza węzły \enquote{\textit{send}} oraz \enquote{\textit{receive}} odpowiedzialne za przesyłanie informacji pomiędzy rozproszonymi zadaniami. Przedstawiono to na rysunku \ref{fig:insert-send-receive-nodes}., na którym główna jednostka zgrupowała parametry modelu w~celu umieszczenia ich wspólnie na pojedynczym serwerze parametrów. Całość przetwarzania grafu pokazano na rysunku \ref{fig:overall-architecture}.

      \begin{figure}[H]
        \centering
        \includegraphics[width=0.99\linewidth]{assets/insert-send-receive-nodes.pdf}
        \caption{Wstawienie węzłów \enquote{\textit{send}} oraz \enquote{\textit{receive}}}
        \label{fig:insert-send-receive-nodes}
      \end{figure}

      \begin{figure}[H]
        \centering
        \includegraphics[width=0.99\linewidth]{assets/overall-architecture.pdf}
        \caption{Wizualizacja całości rozproszonej architektury}
        \label{fig:overall-architecture}
      \end{figure}
      Do zadań \textit{workera} zalicza się:
      \begin{itemize}
        \item obsłużenie żądania pochodzącego z~jednostki głównej;
        \item kolejkowanie i~wykonanie obliczenia zawartego w~otrzymanym podgrafie;
        \item pośredniczenie w~komunikacji pomiędzy różnymi zadaniami.
      \end{itemize}
      \textit{Worker} korzysta z~możliwości zrównoleglania zadań w~postaci wielu rdzeni na CPU lub strumieni na GPU. Za ich pomocą może wysyłać dane pomiędzy dwiema lokalnie zainstalowanymi jednostkami GPU. Inną możliwością jest rozdzielenie zadania pomiędzy CPU i~GPU w~taki sposób, żeby obie jednostki były zajęte cały czas obliczeniami. Dla przykładu CPU może wykonywać transfer kolejnej porcji danych w~czasie, kiedy GPU wykonuje jeszcze obliczenia na poprzednich.

    \subsection{Statyczny graf obliczeniowy}
    \label{subsec:static-graph}
      Biblioteka Tensorflow korzysta z~nurtu tworzenia statycznego, acyklicznego, skierowanego grafu obliczeniowego. Graf można potraktować jako leniwą ewaluację wyrażeń. Względem zachłannej ewaluacji wyrażeń typowych dla programowania umożliwia ona:
      \begin{itemize}
        \item prześledzenie zleconych do wykonania obliczeń przed wykonaniem ciężkich operacji zawartych w~węzłach grafu;
        \item wprowadzenie dodatkowych optymalizacji, na przykład wyliczenie powtarzających się obliczeń tylko raz;
        \item wykonanie tylko części obliczeń;
        \item przechowanie w~pamięci oraz odczytanie w~razie potrzeby częściowych obliczeń;
        \item usunięcie operacji, niepotrzebnych w~czasie wykonywania żądanej operacji;
        \item uspójnienie reprezentacji obliczeń za pomocą odpowiedniego protokołu, dzięki czemu możliwe jest przenoszenie obliczeń pomiędzy różnymi językami programowania;
        \item wykonanie prześledzenia obliczeń dla danego węzła i~wykonanie wstecznej propagacji w~sposób analityczny za pomocą reguły łańcuchowej.
      \end{itemize}

      Przedstawione optymalizacje grafu są szczególnie istotne dla głębokich sieci neuronowych, gdzie staje się ważny czas ich uczenia. Statyczność grafu też niesie ze sobą negatywne skutki. Zalicza się do nich brak możliwości modyfikowania struktury grafu w~trakcie wykonywania obliczeń oraz prześledzenia stosu błędów tworzących się wewnątrz grafu.
      
      W~takim grafie można zdefiniować trzy typy węzłów: wykonujące bazowe operacje matematyczne, posiadające wartość stałą, oraz węzły zmienne, czyli tak zwane \textit{placeholdery}. W~miejsce \textit{placeholderów} ładowana jest wartość przed wykonaniem obliczenia na innych węzłach połączonych z~nimi. Krawędzie pokazują, że dany węzeł jest argumentem funkcji przedstawionej w~węźle, do której krawędź wchodzi. Węzeł posiada informację, jak została policzona wartość zawarta w~nim oraz jak policzyć pochodną względem każdej z~krawędzi \cite{Artzi2017}. Jest to wykorzystywane w~algorytmie wstecznej propagacji. Algorytm jest wykonywany na zasadzie przejścia w~przód po topologii sieci od węzłów reprezentujących dane wejściowe do ostatniego węzła, którego wartość zażądał użytkownik. Graf jest następnie realizowany w~tył i~na każdym z~jego węzłów są obliczane pochodne, aplikowane do węzłów reprezentujących wyuczalne parametry. Korekcja wag jest wyliczana poprzez wymnożenie każdej pochodnej na ścieżce od ostatniego obliczonego węzła do węzła danej wagi. Jeżeli ścieżek jest kilka, to rezultaty na każdej z~nich są ze sobą sumowane.

      Definicja przykładowego grafu obliczeniowego została podana na rysunku \ref{fig:computation-graph}. Na przedstawionym grafie wykonywane jest przemnożenie pojedynczego wejścia $x$~z odpowiadającą mu wagą $w$~i dodanie do rezultatu wyrazu wolnego $b$. Następnie, rezultat jest dodawany do zmiennej $s$~przechowującej agregację wszystkich jego wejść.

      \begin{figure}[!htb]
        \centering
        \includegraphics[width=0.3\linewidth]{assets/computation-graph.pdf}
        \caption{Przykład zdefiniowanego grafu obliczeniowego}
        \label{fig:computation-graph}
      \end{figure}


    \subsection{Rozszerzanie biblioteki o~dodatkowe moduły}
      Aby móc przeprowadzić konieczne badania przedstawione w~rozdziale \ref{chap:experiments}., konieczne było zaimplementowanie neuronów Sigma-If oraz FFA za pomocą biblioteki Tensorflow z~uwzględnieniem wykorzystania technologii CUDA.

      Obecnie, w~bibliotece istnieje ponad 200 standardowych operacji matematycznych manipulujących zawartością tensorów, kontrolujących przepływ oraz zarządzających stanem wykonania. Każda z~tych operacji może posiadać tak zwany kernel, czyli funkcję zoptymalizowaną pod konkretny typ urządzenia. Większość z tych kerneli jest zaimplementowanych przy użyciu biblioteki Eigen \cite{Eigen2010}. Pozwala ona na stworzenie efektywnego, zrównoleglonego kodu dla wielordzeniowych procesorów oraz GPU. Dla GPU zaleca się wykorzystanie cuDNN \cite{Cudnn2014}, biblioteki wyspecjalizowanej pod wykorzystanie w~sieciach neuronowych. Tensorflow wykorzystuje także bibliotekę CUB \cite{Cub2019}, posiadającą implementacje podstawowych prymitywów dla CUDA, w~tym operacje redukcji, skanu, selekcji, sortowania oraz obliczenia histogramu.

      Aby stworzyć operację, która będzie mogła zostać wykorzystana w~języku Python, należało utworzyć w~C++ klasę odpowiadającą celowi operacji. W~przypadku tej pracy, jest to stworzenie klasy wykonującej zależnie od kontekstu kod CPU lub GPU. Klasa dziedziczy po klasie \code{OpKernel}. W~ten sposób, może ona zostać dołączona do grafu obliczeniowego. Klasa zawiera metodę wykonującą operacje, zgodne z~działaniem badanych neuronów, oraz przyjmującą kontekst grafu. Metoda jest uruchamiana za każdym razem, kiedy użytkownik żąda wartości z~węzła zależnego od węzła zawierającego implementowaną operację. Kontekst zawiera informacje na temat grafu oraz przekazywanych danych w~czasie przechodzenia go. Na podstawie otrzymanych danych można określić wymiary danych wyjściowych i~zaalokować pamięć potrzebną na przechowywanie danych wyjściowych. Następnie, konieczne było zaimplementowanie obliczeń zależnych od typu wykorzystywanego urządzenia. Sposób i~rozważania optymalności implementacji obliczeń dla neuronu Sigma-If oraz FFA przedstawiono w~rozdziale \ref{chap:solution}. Implementacja została wykonana dla przejścia w~przód w~grafie oraz przejścia w~tył. Dla wstecznej propagacji utworzono oddzielną klasę. Implementacja metody obliczeniowej dla tej klasy przebiega dokładnie w~ten sam sposób, jak dla przejścia w~przód. Wymaganiem jest implementacja wykonania obliczeń gradientu dla wejść, pochodzących z~poprzedniej warstwy, oraz dla parametrów. Konieczna jest rejestracja zaimplementowanych klas do postaci węzłów. Rejestracja odbywa się za pomocą odpowiednich konstrukcji dostępnych w~bibliotece. Jest ona realizowana oddzielnie dla przejścia w~tył oraz dla przejścia w~przód. Po wykonaniu rejestracji węzła do~postaci Op, konieczne jest podłączenie implementacji do zarejestrowanego węzła. Tak wykonana implementacja i~rejestracja została zbudowana do postaci biblioteki dynamicznej za pomocą dostępnego kompilatora C++ dla CPU oraz NVCC dla CUDA. Zbudowana biblioteka jest możliwa do wykorzystywana w~języku Python. W~tym języku, należało utworzyć dodatkową klasę, która pozwalałaby dodawać przygotowane warstwy neuronów do pozostałych elementów sieci neuronowych oraz przekazywać odpowiednie tensory do wyliczenia gradientu po wejściach oraz parametrach. Tak przedstawiona implementacja neuronów Sigma-If oraz FFA została użyta w~trakcie badań. Takie neurony mogą być łączone z~innymi warstwami dostępnymi w bibliotece Tensorflow.
\end{document}