%! TEX root = ../main.tex
\documentclass[../main.tex]{subfiles}

\begin{document}
  \chapter{Wprowadzenie}
  \label{chap:introduction}

  \section{Motywacja}
    Sieci neuronowe są jednym z~tworów matematycznych wykorzystywanym od wielu lat w~dziedzinie indukcyjnej analizy danych. Pierwszy opis modelu pojedynczego neuronu został stworzony w~1943 roku przez neuropsychologa W. McCullocha oraz matematyka W. Pittsa \cite{McCulloch1943}.  Skonstruowali oni także implementację tego modelu za pomocą dostępnych w~tamtym czasie układów elektronicznych. Tak zwany perceptron prosty, będący implementacją modelu McCullocha i~Pittsa, został spopularyzowany przez F.~Rossenblatta w~1961 roku \cite{VanDerMalsburg1986}. Jego ograniczeniem była możliwość rozwiązywania jedynie prostych problemów separowalnych liniowo. W~związku z~tym, problem XOR był niemożliwy do rozwiązania przy użyciu perceptronu. Odkrycie tego ograniczenia było jednym z~powodów tak zwanej zimy w~dziedzinie sieci neuronowych. Jedyną możliwością przezwyciężenia tego ograniczenia było wprowadzenie wielowarstwowych sieci perceptronowych, które potrafiły modelować dane w~sposób nieliniowy. W~takiej sieci definiuje się warstwy neuronów. Pomiędzy warstwami występują połączenia pomiędzy każdą parą neuronów z~obu warstw. Aby taka sieć mogła być w~skuteczny sposób uczona, konieczne było zaproponowanie algorytmu uczenia. Wykorzystano w~tym celu algorytm wstecznej propagacji. S.~Linnainmaa w~1970 roku w~swojej pracy magisterskiej \cite{Linnainmaa1976} wyprowadził wzory, które pozwalały na propagację błędu względem wszystkich składowych funkcji wyższego rzędu. Dzięki temu możliwe było uczenie parametrów takiej funkcji. P. Werbos \cite{Werbos1974} po raz pierwszy wykorzystał tę propagację błędów w~kontekście uczenia sieci neuronowych. Mimo że sieci mogły być skutecznie uczone skomplikowanych funkcji, dalej nie zyskiwały na popularności ze względu na dalej trwające przekonanie o~słabości tego rozwiązania i~ograniczonej ilości danych treningowych. Dopiero D. Rumelhart i~inni \cite{Rumelhart1986} spopularyzowali wielowarstwowe sieci perceptronowe uczone wsteczną propagacją na większą skalę. Zainteresowanie tym rozwiązaniem wzrosło również za sprawą obniżenia cen komputerów o~wysokiej mocy obliczeniowej. Dzięki temu, uczenie modeli neuronowych mogło przebiegać w~krótszym czasie. Choć w~późniejszym czasie zaproponowano jeszcze szereg innych metod tworzenia sztucznych sieci neuronowych, algorytm wstecznej propagacji błędów jest często wykorzystywany aż do dzisiaj.

    W~obecnych czasach sieci neuronowe są szeroko stosowane w~różnych dziedzinach nauki. Wynika to z~różnorodności zadań, w~których znajdują one zastosowanie: od klasyfikacji obrazu, segmentacji i~detekcji obiektów, po zadania związane z~przetwarzaniem języka naturalnego, w~tym tłumaczenie maszynowe (z ang. \textit{machine translation}). Wielkie koncerny wykorzystują uczenie maszynowe w~swoich produktach ze względu na ogromną ilość danych, którymi dysponują. Facebook wykorzystuje algorytmy opisujące obraz dla automatycznego opisu zawartości zdjęcia i~możliwości przedstawienia tej treści za pomocą syntezatora mowy osobie niewidomej \cite{Verge2016}. Coraz prężniej działa również branża \textit{automotive}, w~której liderem jest Tesla. Za pomocą głębokich modeli sieci neuronowych samochody Tesli są w~stanie jednocześnie przetwarzać obraz z~kamer rejestrujących kolory RGB oraz aparatury Lidar. Obecnie wykorzystywane są kompleksy sieci neuronowych, które jednocześnie wykrywają pas ruchu, pieszych, oraz znaki \cite{Electrek2018}.

    Jednak stosowane sieci neuronowe są wciąż ograniczone. Jednym z~głównych problemów jest konieczność stosowania ogromnych zbiorów danych, aby otrzymywać za ich pomocą rozsądnej jakości wyniki. W~pracy dotyczącej głębokiego uczenia maszynowego, autorstwa G. Hu i~innych \cite{Hu2016}, wykorzystano 10 000 zdjęć i~nazwano taki zbiór \enquote{małym}. W~przypadku zastosowania sieci neuronowych w~medycynie, prawdopodobieństwo skutecznego pozyskania tak licznego zbioru jest niskie. 

    Kolejnym problemem jest brak odporności sieci neuronowych na zakłócenia danych wejściowych. Jak pokazano w~\cite{Cisse2017, Carlini2016, Biggio2017, Moosavi-Dezfooli2015, Kurakin2016} prostymi metodami matematycznymi można tak zmienić dane wejściowe, aby ich modyfikacja była niezauważalna dla człowieka, ale wystarczająca, aby zaburzyć wyniki dawane przez sieć neuronową. Do takich metod modyfikacji danych można zaliczyć kompresję JPEG \cite{Dziugaite2016}. Pojawia się pytanie, czy istnieje sposób wnioskowania w~sieciach neuronowych, który umożliwiłby im ignorowanie szumów wynikających z~wielu różnych procesów stochastycznych wpływających na dane.

    Ważnym problemem jest także moc obliczeniowa, konieczna do wyuczenia dużych sieci neuronowych oraz ich późniejszego wykorzystania. W~przypadku ogromnych kompleksów algorytmów, takich jak AlphaZero \cite{Silver2017}, w~czasie samej ewaluacji pojedynczego ruchu w~grze w~szachy wykorzystywano 4~jednostki TPU o~mocy przewyższającej znacząco moc GPU oraz 44-rdzeniowego CPU. W~związku z~czym, pojawia się wątpliwa możliwość odtworzenia rezultatów zawartych w~publikacji na temat AlphaZero \cite{Silver2017}. Należy zadać sobie też pytanie, czy naprawdę istnieje konieczność wykorzystania tak silnego komputera, aby uzyskać ten sam wynik. Znaczące ograniczenie liczby operacji zmiennoprzecinkowych na sekundę, przy zachowaniu podobnych struktur sieci i~skuteczności algorytmów, pozwoliłoby na wykorzystanie słabszego GPU w~tych samych badaniach.

    Kolejną przeszkodą jest brak zdolności sieci do analizy tylko najbardziej istotnego obszaru danych wejściowych. Człowiek w~czasie obserwowania otoczenia skupia się na jednym obszarze naraz. Nagłe zdarzenie w~obszarze pola widzenia powoduje koncentrację wzroku w~danym punkcie, przy czym informacje nieobejmujące tego zdarzenia są zazwyczaj ignorowane. To zjawisko nazywa się kierunkowaniem uwagi i~jest ważnym elementem w~procesie filtrowania informacji. Dzięki temu, tylko najważniejsze informacje docierają do głębszych partii mózgu oraz nie następuje jego przeciążenie od nadmiaru napływających bodźców. Tym zjawiskiem interesują się zarówno informatycy, psychologowie oraz biologowie \cite{Francuz2000}. Podejrzewa się, że opisywany mechanizm znajduje się wewnątrz neuronów i~to one odpowiadają za filtrowanie i~koncentrowanie wokół konkretnej informacji \cite{LaBerge1990}. Pomimo tych założeń, obecnie wiele algorytmów głębokiego uczenia maszynowego wykorzystuje zewnętrzne moduły oraz parametryzowane, wyuczalne funkcje, które mają za zadanie odpowiadać systemowi kierunkowania uwagi \cite{Bahdanau2014, Xu2015, Wang2017}. Mimo zauważalnej poprawy osiąganych wyników przedstawionych w~tych pracach, wątpliwa jest odporność takiego rozwiązania na wspomniane wyżej ataki. Pojedynczym atakiem na wyuczoną sieć neuronową nazywamy taką zmianę danych wejściowych, która nie zmienia ich obrazu, natomiast powoduje zmianę decyzji wyznaczanej przez sieć neuronową.

    W~związku z~powyższym, proponowano już wielokrotnie modyfikacje sposobu agregacji sygnału wejściowego przez neurony znajdujące się w~sieciach \cite{Mel1989, Durbin1989, Fahlman1990, Mel1992}. Jedną z~modyfikacji jest wielokrokowa agregacja grup sygnałów przez neurony nazwany Sigma-If \cite{Huk2012}. Agregacja w~takim neuronie jest przeprowadzana do momentu osiągnięcia określonego poziomu pobudzenia. Pozwala to na kierunkowanie uwagi, które jest zakodowane w~sposobie działania tych neuronów. Obecnie istnieją jedynie implementacje w~środowiskach rozproszonych \cite{Janusz2018} na platformie H2O \cite{H2O2015}. Do tej pory nie zaproponowano efektywnej implementacji, która działałaby na kartach graficznych. 
    
    W~następstwie informacji zawartych wyżej, w~obrębie pracy wykonano następujące czynności. Opisano oraz zrealizowano implementację neuronu Sigma-If w~środowisku CUDA przy wykorzystaniu biblioteki Tensorflow dla języka Python. Zbadano także rozszerzenie, zaproponowane przez promotora tej pracy, neuronów Sigma-If poprzez wprowadzenie tak zwanego współczynnika zanikania. Nazwano je neuronami \textit{Fading Field Attention} (FFA). Przebadane zostało również rozszerzenie FFA polegające na zastosowaniu odmiennych współczynników zanikania przy wnioskowaniu w~przód oraz wstecznej propagacji. Sieci skonstruowane za pomocą jednego z~tych typów neuronów zalicza się do grupy sieci kontekstowych. Zostały one przeanalizowane pod względem skuteczności względem tradycyjnych sieci neuronowych. Sprawdzono także, czy wykorzystują one tylko część całej informacji wejściowej w~celu prawidłowej predykcji. Zbadano także wydajność implementacji. Otrzymane wyniki wskazują na to, że badane neurony mogą zastąpić stosowane obecnie tradycyjne sieci neuronowe.

  \section{Cel i~zakres}
    Celem pracy jest zbadanie właściwości wybranych kontekstowych sieci neuronowych przy wykorzystaniu GPU oraz porównanie skuteczności tych modeli w~połączeniu z~wybranymi architekturami sieci płytkich i~głębokich. W~skład tego zadania wchodzi:
    \begin{enumerate}
      %~\item Poznanie architektury kart graficznych oraz środowiska CUDA, w~którym sieci zostały zaimplementowane. Zapoznanie się także z~możliwościami rozszerzenia biblioteki Tensorflow o~dodatkowy moduł zawierający implementację sieci kontekstowych w~celu uproszczenia przeprowadzenia badań.
      %~\item Implementacja warstwy zawierającej neurony Sigma-If wraz z~algorytmem wstecznej propagacji. Powinna zawierać zarówno implementację na CPU jak i~GPU w~językach odpowiednio C++ i~C.
      %~\item Implementacja tradycyjnej metody agregacji sygnału wejściowego przedstawionego w~\cite{Huk2012} oraz agregacji sygnału przy uwzględnieniu współczynnika zanikania. 
      %~\item Stworzenie interfejsu zaimplementowanego modułu do języka python na potrzeby wykorzystania przez bibliotekę Tensorflow.
      %~\item Implementacja środowiska badawczego, w~którym będzie możliwości doboru architektury sieci, badanego zbioru danych oraz doboru wartości hiperparametrów poszczególnych komponentów, w~tym liczba epok, wielkość \textit{minibatcha}, współczynnik uczenia, optymalizator i~inne.
      \item Zbadanie skuteczności i~aktywności połączeń neuronowych wybranych sieci kontekstowych oraz zestawienie ich z~tradycyjnymi sieciami w~pełni połączonymi. Badania uwzględniają neurony Sigma-If, model kierunkowania uwagi ze współczynnikiem zanikania, w~tym możliwości stosowania różnych współczynników zanikania w~propagacji przód oraz w~tył.
      \item Analiza wpływu doboru wartości hiperparametrów na otrzymywane wyniki.
      \item Zbadanie skuteczności zdefiniowanych sieci wykorzystujących ekstraktor cech w~postaci sieci konwolucyjnej.
      \item Sprawdzenie odporności sieci w~wersji z~wyuczalnym ekstraktorem cech i~bez wyuczalnego ekstraktora cech na addytywny szum z~rozkładu normalnego.
      \item Zestawienie otrzymanych wyników i~porównanie tradycyjnych neuronów oraz neuronów Sigma-If pod względem skuteczności oraz szybkości działania na jednostkach GPU i~CPU oraz wyciągnięcie wniosków z~przeprowadzonych badań.
      \item Analiza optymalności zaproponowanej implementacji w~środowisku CUDA.
    \end{enumerate}

  \section{Zakres pracy}
    W~rozdziale \ref{chap:introduction}. została zdefiniowana motywacja oraz cel stworzenia pracy. Znajdują się w~nim także informacje na temat wykorzystanych narzędzi, pojęć używanych w~obrębie niniejszego tekstu oraz dodatkowe odnośniki do repozytoriów, dzięki którym istnieje możliwość odtworzenia badań. W~następnym rozdziale zawarto rys historyczny sieci neuronowych, zaczynając od pierwszego modelu neuronu, przechodząc do głębokich sieci neuronowych i ich zastosowań. Zawarto w~nim również informacje na temat postępów w~badaniu systemów uwagi i~wykorzystaniu ich w~kontekście sieci neuronowych. Rozdział kończy szeroki opis prac dotyczących systemu kierunkowania uwagi zawartego w~samych neuronach oraz ich rodzaje. W~rozdziale \ref{chap:environment}. opisano środowisko CUDA oraz bibliotekę Tensorflow. Rozdział \ref{chap:solution}. zawiera opis propozycji zrównoleglonej implementacji agregacji neuronów Sigma-If na CPU oraz GPU w~C++ oraz, jak zostało to zintegrowane z~językiem Python. W~rozdziale \ref{chap:experiments}. opisano metodykę badań, w~tym zastosowane metryki, zbiory danych, rozważane modele sieci neuronowych, badane hiperparametry oraz opis przeprowadzonych eksperymentów. Do każdego z~eksperymentów dołączono dokładny opis stosowanych wartości parametrów, otrzymane wyniki oraz ich analizę. W~ostatnim z~rozdziałów zapisano autorskie przemyślenia dotyczące wyników pracy oraz wnioski, które posłużyły do konstrukcji dalszych planów rozwoju sieci kontekstowych.

  \section{Zastosowane narzędzia i~biblioteki}
    \paragraph{Anaconda 4.6.7} \ \\
      Środowisko naukowe dla języka Python. Pozwala w~łatwy sposób zarządzać pakietami instalowanymi dla tego języka oraz izolowalnymi środowiskami, gdzie każde środowisko może zawierać inne wersje tych samych pakietów. Dzięki temu całe środowisko badawcze wykorzystane w~pracy jest łatwo reprodukowalne.
    \paragraph{C CUDA 9.0} \ \\
      Interfejs środowiska CUDA dla języka C. Pozwala w~sposób niemal przeźroczysty dla programisty pisać jednocześnie kod dla CPU (\textit{host}) oraz dla GPU (\textit{device}). Wprowadza do języka kilkanaście deklaracji, które pozwalają jasno wskazać, które funkcje i~zmienne będą przechowywane i~wykonywane na GPU. Pozwala także na parametryzowane uruchamianie programów napisanych pod GPU, dzięki czemu można wskazać, w~jaki sposób będą wykorzystywane zasoby dostępne na karcie graficznej.
    \paragraph{Docker oraz docker-compose - 18.09.0 i~1.22.0} \ \\
      System kontenerowy, który pozwala na uruchamianie systemy operacyjny jako podsystem. Dzięki temu można wyizolować środowisko bazujące na innym oprogramowaniu niż system \textit{hostujący}. Wykorzystywany jest do kompilowania implementacji modułu Tensorflow w~języku C++, który wymaga specyficznych wersji poszczególnych komponentów oprogramowania.
    \paragraph{Git 2.17.1} \ \\
      System kontroli wersji stosowany w~procesie implementacji środowiska badawczego. Za jego pomocą i~rozszerzenia Git-LFS są przechowywane wszystkie komponenty potrzebne do przeprowadzenia badań i~odtworzenia wyników. Na wyspecyfikowanym repozytorium przechowywane są wersje zbiorów danych, które wykorzystano w~trakcie badań.
    \paragraph{NVCC 9.0} \ \\
      Kompilator kodu źródłowego napisanego w~CUDA. Wykorzystywany jest do skompilowania implementacji Sigma-If oraz FFA dla GPU.
    \paragraph{Python 3.6.8} \ \\
      Interpretowalny, dynamicznie typizowany język programowania, który pozwala w~prosty sposób wykonać prototypowanie oprogramowania oraz budowanie skomplikowanych systemów. Przy jego pomocy zaimplementowano całe środowisko badawcze oraz instalowalny pakiet, rozszerzający możliwości biblioteki Tensorflow. Pakiet zawiera również testy jednostkowe, w~których badana jest poprawność wykonywanych obliczeń w~neuronach Sigma-If oraz FFA.
    \paragraph{Tensorflow 1.9.0} \ \\
      Specjalistyczna biblioteka dla języka Python, która służy do konstruowania funkcji matematycznych w~formie grafu obliczeniowego. Funkcje matematyczne obejmują także głębokie sieci neuronowe. Wewnątrz biblioteki, istnieje możliwość przeprowadzania automatycznego liczenia gradientu, dzięki czemu użytkownik potrzebuje jedynie zadeklarować sposób wnioskowania w~przód, natomiast uczenie będzie przeprowadzane automatycznie za pomocą wstecznej propagacji. Obliczenia są wykonywane za pomocą biblioteki napisanej w~języku C++ oraz C, gdzie są one jeszcze dodatkowo optymalizowane. Programy napisane w~Tensorflow mogą być wykonywane zarówno na CPU jak i~GPU.


  \section{Słownik pojęć}
    \paragraph{Instrukcja SIMD} \ \\
      Oznacza instrukcję procesora, która pozwala w~pojedynczym kroku taktowania wykonać zrównoleglone obliczenia na wektorze lub macierzy (z ang. \textit{Single Instruction Multiple Data}).
    \paragraph{Kernel} (w~kontekście CUDA) \ \\
      Jest to funkcja w~języku C++ oznaczona dyrektywą \code{\_\_globa\_\_}, oznaczającą funkcję dystrybuowaną na wątki na karcie graficznej.
    \paragraph{Kernel} (w~kontekście biblioteki Tensorflow) \ \\
      Specjalistyczna funkcja programu w~bibliotece Tensorflow, której zadaniem jest wykonanie obliczeń na danych na wyjściu i~zwrócenie wyniku na wyjście. Może dotyczyć zarówno implementacji CPU oraz GPU.
    \paragraph{Notacja 1-na-K} \ \\
      Notacja, w~której zamiast wartości liczbowej reprezentującej daną klasę, przedstawia się jako wektor zer i~pojedynczej jedynki, gdzie jedynka znajduje się na indeksie odpowiadającym numerowi klasy.
    \paragraph{Op} \ \\
      Podstawowa jednostka budulcowa biblioteki Tensorflow. Pojedynczy Op jest zdefiniowany w~kodzie C++ biblioteki i~jest kompilowany do postaci dynamicznej biblioteki. Dla każdego Op konieczne jest zaimplementowanie sposobu przetwarzania wejścia w~przód oraz w~tył, aby była możliwość dołączenia go w~grafie obliczeniowym.
    \paragraph{\textit{Overfitting}} \ \\
      Zjawisko polegające na zbytnim dopasowaniu określonego modelu do danych. Powoduje to dużą różnicę błędu modelu na zbiorze treningowym oraz walidacyjnym. Aby temu przeciwdziałać, stosuje się różne techniki regularyzacji. 
    \paragraph{Sieci kontekstowe} \ \\
      Sieci wykorzystujące neurony przetwarzające tylko część informacji wejściowych (analizujące tylko pewien kontekst).
    \paragraph{Spłaszczenie macierzy} \ \\
      Przedstawienie macierzy o~wymiarach $N \times M$ jako wektor o~liczbie elementów równej $NM$.
    \paragraph{Walidacja krzyżowa (kroswalidacja)} \ \\
      Proces uczenia, w~którym zbiór danych jest dzielony na N~równolicznych, rozłącznych podzbiorów. Następnie, wykorzystuje się $N-1$ podzbiorów jako zbiór treningowy i~$N$-ty jako walidacyjny. Proces uczenia jest powtarzany dla każdego z~$N$ podzbiorów. Następnie, możliwe jest wyliczenie statystyk z~otrzymanych $N$~błędów.
    \paragraph{Wnioskowanie w~przód w~sieci neuronowej} \ \\
      Wykonanie przejścia w~przód w~sieci neuronowej dla podanych na wejściu danych. Jest to równoznaczne z~predykcją decyzji.
    \paragraph{Wnioskowanie w~tył w~sieci neuronowej} \ \\
      Wykonanie przejścia w~tył w~sieci neuronowej dla danych wyliczonych na wyjściu z~niej. Jest to równoznaczne z~wykonaniem pojedynczego kroku algorytmu wstecznej propagacji, opisanego w~rozdziale \ref{chap:neural-networks}.
    \paragraph{Zbiór treningowy, walidacyjny, testowy} \ \\
      Zbiory wykorzystywane w~trakcie konstrukcji wyuczalnego modelu. Treningowy służy do optymalizacji parametrów modelu. Walidacyjny pozwala stwierdzić skuteczność modelu na próbkach, które nie były widoczne w~czasie uczenia, oraz dopasować hiperparametry dla zwiększenia skuteczności. Testowy jest ostateczną weryfikacją skuteczności uczonego modelu. W~przypadku braku takiego podziału dostępnych, stosuje się kroswalidację. 

  \section{Oznaczenia stosowane w~obrębie pracy}
    \begin{itemize}
      \item[$||\mathbf{v}||$] długość $L_2$ wektora
      \item[$m_{ij}$] element macierzy $i$-tym wierszu i $j$-tej kolumnie
      \item[$v_i$] element wektora o~indeksie $i$ 
      \item[$\mathcal{L}$] funkcja straty
      \item[$|\mathbf{v}|$] liczba elementów wektora
      \item[$\mathbf{M}$] macierz
      \item[$\mathbb{R}^{W \times H}$] macierz o wymiarach $W$ na $H$ posiadająca wartości ze zbioru liczb rzeczywistych
      \item[$\alpha$ lub $u$] skalar
      \item[$\mathbf{v}$] wektor
      \item[$\mathbf{0}\text{, }\mathbf{1}$] wektor lub macierz wypełniona odpowiednio zerami lub jedynkami
      \item[\ensuremath{[0, 1]}] zamknięty zakres wartości od 0 do 1
      \item[$\mathcal{D}$] zbiór
      \item[$\mathbb{R}$] zbiór liczb rzeczywistych
    \end{itemize}

  \section{Dostęp do środowiska badawczego}
    W~czasie tworzenia pracy i~wykonywania badań stworzono 3~repozytoria w~systemie kontroli wersji Git.

      \begin{description}
          \item[SigmaIf-Building] 
          Zawiera implementację CPU oraz GPU tak zwanego Op biblioteki Tensorflow w~języku C++ oraz C. Kompilacja jest przeprowadzana wewnątrz kontenera Dockera. Następnie skompilowany dynamicznie moduł jest kopiowany do repozytorium SigmaIf-Tensorflow\footnote{\url{https://gitlab.com/kacper1095/sigmaif-building}}.
          \item[SigmaIf-Tensorflow] 
          Zawiera definicję klasy funkcyjnej w~języku Python, która wykorzystuje skompilowany moduł. Dodatkowo, zawarto tam metody grupowania połączeń neuronowych oraz testy jednostkowe weryfikujące poprawność wykonywania obliczeń wewnątrz skompilowanego Op. Całość opakowano w~instalowalny pakiet dla języka Python\footnote{\url{https://gitlab.com/kacper1095/sigmaif-tensorflow}}.
          \item[SigmaIf-Testing] Zawiera całe środowisko badawcze, za pomocą którego uzyskano wyniki przedstawione w~rozdziale \ref{chap:experiments}. Posiada definicję architektur sieci neuronowych oraz wykorzystane zbiory danych. Aby móc wykorzystać to repozytorium w~prawidłowy sposób, należy wykonać procedury instalacyjne zawarte w~SigmaIf-Building oraz SigmaIf-Tensorflow\footnote{\url{https://gitlab.com/kacper1095/sigmaif-testing}}.
      \end{description}
  
\end{document}